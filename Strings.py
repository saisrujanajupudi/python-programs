a = "ThIS Is A STrinG"
k=0
l=0
for i in a:
    if i.isupper():
        k=k+1
    elif i.islower():
        l=l+1
print("String: %s"%a)
print('string has %d uppercase characters'%k)
print('string has %d lowercase characters'%l)
print("String joining: ",end="")
print("-".join(a.lower().split(" ")))
print('substring: %s'%a[0:4])
print('concatenation: %s'%a[0:4]+a[10:16])
print("Length of the string =",len(a))
print("Converting the first character to uppercase:",a.capitalize())