a = [22,56,45,47,85,36]
c = [1,3,5,6,4,2]
print("List a:",a)
print(max(a),'is the maximum value')
print(min(a),'is the minimum value')
print("List in ascending order:",sorted(a))
b = []
for i in reversed(a):
    b.append(i)
print("List in reverse order:", b)
print("Number of elements in a =",len(a))
a.insert(1,64)
print("List after inserting a numner at index 1:",a)
a.extend(c)
print("List after adding another list to it:",a)
a.clear()
print("List after removing all the elements in it:",a)
print("Repetition of list c:",c*2)